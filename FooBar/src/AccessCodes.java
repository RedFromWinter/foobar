import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

public class AccessCodes {
	/**
	 * 
	 * Your job is to compare a list of the access codes and find the number of
	 * distinct codes, where two codes are considered to be "identical" (not
	 * distinct) if they're exactly the same, or the same but reversed. The access
	 * codes only contain the letters a-z, are all lowercase, and have at most 10
	 * characters. Each set of access codes provided will have at most 5000 codes in
	 * them.
	 * 
	 * For example, the access code "abc" is identical to "cba" as well as "abc."
	 * The code "cba" is identical to "abc" as well as "cba." The list ["abc,"
	 * "cba," "bac"] has 2 distinct access codes in it.
	 * 
	 * Write a function answer(x) which takes a list of access code strings, x, and
	 * returns the number of distinct access code strings using this definition of
	 * identical.
	 * 
	 */
	
	/*
	 * Test cases ==========
	 * 
	 * Inputs: (string list) x = ["foo", "bar", "oof", "bar"] Output: (int) 2
	 * 
	 * Inputs: (string list) x = ["x", "y", "xy", "yy", "", "yx"] Output: (int) 5
	 */

	@Test
	public void test_1() {
		for (int i = 0; i < 1; i++) {
			final int example1 = answer(new String[] { "foo", "bar", "oof", "bar" });
			System.out.println(example1);
			final int example2 = answer(new String[] { "x", "y", "xy", "yy", "", "yx" });
			System.out.println(example2);
			final int test1 = answer(new String[] { "aa", "aa", "aa" });
			System.out.println(test1);
		}
	}

	public static int answer(final String[] codes) {
		final Set<AccessCode> distinct = new HashSet<>();
		for (final String code : codes) {
			distinct.add(new AccessCode(code));
		}
		return distinct.size();
	}

	public static class AccessCode {

		private final String code;

		public AccessCode(final String code) {
			this.code = code;
		}

		public String getCode() {
			return code;
		}

		@Override
		public int hashCode() {
			// Generate a quick hash honoring the symmetry requirement of access codes.
			// Given the code of "foo" and byte values for 'f' = 3, 'o' = 4, and 'd' = 5.
			//
			//
			// letter		offset from midpoint		byte value		product		fullHash
			//	f			2							3				6
			//	o			1							4				4
			//	o			2							4				8			18
			//
			//  o			2							4				8
			//  o			1							4				4
			//	f			2							3				6			18
			//
			//  o			2							4				8			
			//  f			1							3				3	
			//  o			2							4				8			19
			//
			//  f			2							3				6			
			//  o			1							4				4	
			//  o			1							4				4			
			//	d			2							5				10			24
			
			final byte[] byteCharArray = code.getBytes();
			final int length = byteCharArray.length;
			final int midpoint = length / 2;
			final boolean isEven = length % 2 == 0;

			int sum = 0;
			if (isEven) {
				sum = sum + calcHashPart(byteCharArray, 0, midpoint, midpoint, 0);
				sum = sum + calcHashPart(byteCharArray, midpoint, length, midpoint, 1);
			} else {
				sum = sum + calcHashPart(byteCharArray, 0, length, midpoint, 1);
			}
			return sum;
		}

		private int calcHashPart(byte[] byteArray, int start, int stop, int midpoint, int adjustment) {
			int sum = 0;
			for (int i = start; i < stop; i++) {
				// distance from midpoint with optional adjustment for even length code cases
				final int symetricOffset = Math.abs(i - midpoint) + adjustment;
				final int intChar = (int) byteArray[i];
				sum = sum + symetricOffset * intChar;
			}
			return sum;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			AccessCode other = (AccessCode) obj;
			if (code == null) {
				if (other.code != null)
					return false;
			}
			boolean isEqual = code.equals(other.code);
			if (!isEqual) {
				final String reverse = new StringBuilder(code).reverse().toString();
				isEqual = reverse.equals(other.code);
			}
			return isEqual;
		}
	}
}

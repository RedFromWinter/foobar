import org.junit.Test;

public class RotateMatrix {
	@Test
	public void test_1() {
		/* ROTATE CW 90
		 * 	IN:
		 * 			A B C D
		 * 			E F G H
		 * 			I J K L
		 * 			M N O P
		 * 
		 * 	OUT:	
		 * 			M I E A
		 * 			N J F B
		 * 			O K G C
		 * 			P L H D
		 */
		final char[][] in = 	{
								{'a', 'b', 'c', 'd'},
								{'e', 'f', 'g', 'h'},
								{'i', 'j', 'k', 'l'},
								{'m', 'n', 'o', 'p'},
								};
		final char[][] out = answer(in);
		displayMatrix(out);
	}
	
	@Test
	public void test_2() {
		/* ROTATE CW 90
		 * 	IN:
		 * 			A B C D E
		 * 			F G H I J
		 * 			K L M N O
		 * 			P Q R S T
		 * 			U V W X Y
		 * 
		 * 	OUT:	
		 * 			U P K F A
		 * 			V Q L G B
		 * 			W R M H C
		 * 			X S N I D
		 * 			V T O J E
		 */
		final char[][] in = 	{
								{'a', 'b', 'c', 'd', 'e'},
								{'f', 'g', 'h', 'i', 'j'},
								{'k', 'l', 'm', 'n', 'o'},
								{'p', 'q', 'r', 's', 't'},
								{'u', 'v', 'w', 'x', 'y'},
								};
		final char[][] out = answer(in);
		displayMatrix(out);
	}

	// Function to print the matrix
	private static void displayMatrix(char[][] in) {
		final int n = in.length;
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++)
				System.out.print(" " + in[i][j]);

			System.out.print("\n");
		}
		System.out.print("\n");
	}
	
	private char[][] answer(char[][] in) {
		final int n = in.length;
		final char[][] out = new char[n][n]; // assume symmetric
		for (int x = 0; x <= n / 2; x++) {
			for(int y = 0; y < n - x- 1; y ++)
			{
				out[x][y] = in[n-1-y][x]; 
				out[y][n-1-x] = in[x][y];
				out[n-1-x][n-1-y] = in[y][n-1-x];
				out[n-1-y][x] = in[n-1-x][n-1-y];
			}
		}
		return out;
	}
}

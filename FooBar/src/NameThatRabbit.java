import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.junit.Test;

public class NameThatRabbit {
	
	/**
	 * Name that rabbit ================
	 * 
	 * "You forgot to give Professor Boolean's favorite rabbit specimen a name? You
	 * know how picky the professor is! Only particular names will do! Fix this
	 * immediately, before you're... eliminated!"
	 * 
	 * Luckily, your minion friend has already come up with a list of possible
	 * names, and we all know that the professor has always had a thing for names
	 * with lots of letters near the 'tail end' of the alphabet, so to speak. You
	 * realize that if you assign the value 1 to the letter A, 2 to B, and so on up
	 * to 26 for Z, and add up the values for all of the letters, the names with the
	 * highest total values will be the professor's favorites. For example, the name
	 * Annie has value 1 + 14 + 14 + 9 + 5 = 43, while the name Earz, though
	 * shorter, has value 5 + 1 + 18 + 26 = 50.
	 * 
	 * If two names have the same value, Professor Boolean prefers the
	 * lexicographically larger name. For example, if the names were AL (value 13)
	 * and CJ (value 13), he prefers CJ.
	 * 
	 * Write a function answer(names) which takes a list of names and returns the
	 * list sorted in descending order of how much the professor likes them.
	 * 
	 * There will be at least 1 and no more than 1000 names. Each name will consist
	 * only of lower case letters. The length of each name will be at least 1 and no
	 * more than 8.
	 * 
	 * Languages =========
	 * 
	 * To provide a Python solution, edit solution.py To provide a Java solution,
	 * edit solution.java
	 * 
	 * Test cases ==========
	 * 
	 * Inputs: (string list) names = ["annie", "bonnie", "liz"] Output: (string
	 * list) ["bonnie", "liz", "annie"]
	 * 
	 * Inputs: (string list) names = ["abcdefg", "vi"] Output: (string list) ["vi",
	 * "abcdefg"]
	 * 
	 * 
	 */
	
	@Test
	public void test()
	{
		final String[] result = answer(new String[] {"annie", "bonnie", "liz"});
		print(result); // bonnie, liz, annie
		final String[] result2 = answer(new String[] {"abcdefg", "vi"});
		print(result2); // vi, abcdefg
		final String[] result3 = answer(new String[] {"AL", "CJ"});
		print(result3); // CJ, AL
	}
	
	private void print(String[] result) {
		for (final String str : result)
		{
			System.out.print(str + ", ");
		}
		System.out.println("");
	}
	
	//

	public static String[] answer(String[] names) {
		final List<String> listNames = Arrays.asList(names);
		Collections.sort(listNames, new Ranking());
		final String[] result = new String[names.length];
		return listNames.toArray(result);
	}
	
	public static class Ranking implements Comparator<String> {
		private static final int BASE = (int) "a".getBytes()[0] - 1;
		
		@Override
		public int compare(String o1, String o2) {
			int o1Val = rankString(o1);
			int o2Val = rankString(o2);
			int result = o2Val - o1Val;
			if (result != 0)
				return result;
			return o1.compareTo(o2) * -1;
		}
		
		private int rankString(String str)
		{
			int strVal = 0;
			for(final byte val : str.getBytes())
			{
				strVal = strVal + ((int) val - BASE);
			}
			return strVal;
		}
	}
}

import org.junit.Test;
import org.w3c.dom.Node;

public class IndustryBigO {
	/**
	 * Note: In academic, there is 
	 * Big0 = upper bound
	 * BigO(theta) lower bound
	 * BigO(omega) tight bound
	 * 
	 * In industry, BigO and BigO(omega) mashed up a bit and constants are generally dropped.
	 * 
	 */
	
	public void example1()
	{
		int[] array = new int[100];
		int sum = 0;
		int product = 1;
		for (int i = 0; i < array.length; i++) {
			sum += array[i];
		}

		for (int i = 0; i < array.length; i++) {
			sum *= array[i];
		}
		
		// really this is O(2N) but we drop constant so its O(N)
	}
	
	public void example2()
	{
		int[] array = new int[100];
		for (int i = 0; i < array.length; i++) {
			for (int j = 0; j < array.length; j++) {
				// some fancy calculation
			}
		}
		// O(N^2)
	}
	
	public void example3()
	{
		int[] array = new int[100];
		for (int i = 0; i < array.length; i++) {
			for (int j = i + 1; j < array.length; j++) {
				// some fancy calculation
				
				// analysis: math geek
				// (N-1) + (N-2) + (N-3) + .. + 2 + 1 + 0
				// if we analyze as pairs... then we take result * n/2 
				// 	#	a		b		a + b
				//	1	0		N - 1	N - 1
				//	2	1		N - 2	N - 1
				//  3   2		N - 3	N - 1
				//
				// n/2 * n-1 = n(n-1)/2 => O(n^2)
				
				// analysis v2: english geek
				// there are NxN pairs of i and j, half of those will have i < j, and the other j < i
				// => n^2/2 => O(n^2)
				
				// analysis v3: visual geek
				// 	(0, 1)	(0, 2)	(0, 3)	(0, 4)	(0, 5)
				//			(1, 2)	(1, 3)	(1, 4)	(1, 5)
				//					(2, 3)	(2, 4)	(2, 5)
				//							(3, 4)	(3, 5)
				//									(4, 5)
				// => looks like a NxN matrix divided by 2 => n^2/2 => O(n^2)		
			}
		}
	}
	
	public void example4()
	{
		int[] arrayA = new int[100];
		int[] arrayB = new int[100];
		for (int i = 0; i < arrayA.length; i++)
		{
			for (int j = 0; j < arrayB.length; j++)
			{
				if (arrayA[i] < arrayB[j])
				{
					// something fancy
				}
			}
		}
		
		// O(A*B)
		// the if statement is a distraction it is O(1) since it is constant statement
	}
	
	public void example5()
	{
		int[] arrayA = new int[100];
		int[] arrayB = new int[100];
		for (int i = 0; i < arrayA.length; i++)
		{
			for (int j = 0; j < arrayB.length; j++)
			{
				for (int k = 0; k < 1000; k++)
				{
					// something fancy
				}
			}
		}
		
		// O(A*B)
		// the third inner loop is a distraction it is O(1) since it is constant statement
	}
	
	public void example6()
	{
		int[] array = new int[100];
		for (int i = 0; i < array.length; i++)
		{
			int other = array.length - i - 1;
			int temp = array[i];
			array[i] = array[other];
			array[other] = temp;
		}
		
		// O(N) 
		// drop the constant of 1/2
	}
	
	public void example7()
	{
		// sort each string in an array, and then sort the array
		// let a be the elements of the array
		// let s be the longest string
		//
		// given it takes O(s log s) to sort each string
		// and we sort it a times=> O(a*s log s)
		//
		// given it takes O(a log a) to sort the array
		// and each string compare takes s
		// => O(s*a log a)
		//
		// product is => O(a*s(loga + logs))
	}
	
	@Test
	public void test()
	{
		System.out.println(isOverFiveCharacters("abcd"));
		System.out.println(isOverFiveCharacters("abcdefg"));
		System.out.println(isOverFiveCharacters(null));
	}
	
	public boolean isOverFiveCharacters(String s)
	{
	    return s == null ? false : s.length() > 5;
	}
	
	// example 8
	public int sum(Node node)
	{
		if (node == null)
			return 0;
		return sum(node.getFirstChild()) + Integer.valueOf(node.getNodeValue()) + sum(node.getLastChild());
		
		// each node on the tree visited once in a recursive fashion therefore O(n)
		//
		//
	}
	
	// example 10
	public boolean isPrime(int n)
	{
		for ( int x = 2; x * x < n; x++) {
			if (n % x == 0)
			{
				return false;
			}
		}
		return true;
		// O(sqrt(n))
	}
	
	// example 11
	public int factorial(int n)
	{
		if (n < 0) {
			return -1;
		} else if (n == 0) {
			return 1;
		} else {
			return n * factorial(n-1);
		}
		// given 4
		// 4, 3, 2, 1
		// O(n)
	} 
	
	// example 12
	public void permutation(String str)
	{
		permutation("", str);
	}
	private void permutation(String prefix, String str) {
		if (str.length() == 0) {
			System.out.println(prefix);
		} else {
			for (int i = 0; i < str.length(); i++)
			{
				String rem = str.substring(0, i) + str.substring(i + 1);
				permutation(rem, prefix + str.charAt(i));
			}
		}
		// this one is tricky!
		// look at how many times permutation is called
		// say we have a string with 7 character slots
		// we have 7 choices for first slots, 6 for second slot and so on
		// 7 * 6 * 5 * 4 * 3 * 2 * 1 = 7!
		//
		// permutation called n! (leaves or final answer)
		//	n calls to hit a leaf OR n calls before base case reached
		// n * n!
		//
		// string concat O(N)
		// string print (O(N))
	}
	
	// example 14
	public int fib(int n)
	{
		if (n <= 0) return 0;
		else if (n == 1) return 1;
		return fib(n - 1) + fib(n - 2);
		
		// 2 choices, recurssive nature =? O(branches^depth) => 2^n
	}
	
	// example 15
	public void allFib(int n)
	{
		for (int i = 0; i < n; i++)
		{
			System.out.println(i + ": " + fib(i));
		}
		// this is not n2^n because n reduces
		// this is the sum of powers of 2 again => 2^n+1
		// Note: add the sums in binary, then realize sum is about the value of the next power of 2
		//
		// could use a memo which is a technique to optimize exponential time recursive algorithms
	}
	
}

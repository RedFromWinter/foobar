import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

public class SaveTheBetaRabit {
//	Save Beta Rabbit
//	================
//
//	Oh no! The mad Professor Boolean has trapped Beta Rabbit in an NxN grid of rooms. In the center of each room (except for the top left room) is a hungry zombie. In order to be freed, and to avoid being eaten, Beta Rabbit must move through this grid and feed the zombies.
//
//	Beta Rabbit starts at the top left room of the grid. For each room in the grid, there is a door to the room above, below, left, and right. There is no door in cases where there is no room in that direction. However, the doors are locked in such a way that Beta Rabbit can only ever move to the room below or to the right. Once Beta Rabbit enters a room, the zombie immediately starts crawling towards him, and he must feed the zombie until it is full to ward it off. Thankfully, Beta Rabbit took a class about zombies and knows how many units of food each zombie needs be full.
//
//	To be freed, Beta Rabbit needs to make his way to the bottom right room (which also has a hungry zombie) and have used most of the limited food he has. He decides to take the path through the grid such that he ends up with as little food as possible at the end.
//
//	Write a function answer(food, grid) that returns the number of units of food Beta Rabbit will have at the end, given that he takes a route using up as much food as possible without him being eaten, and ends at the bottom right room. If there does not exist a route in which Beta Rabbit will not be eaten, then return -1.
//
//	food is the amount of food Beta Rabbit starts with, and will be a positive integer no larger than 200.
//
//	grid will be a list of N elements. Each element of grid will itself be a list of N integers each, denoting a single row of N rooms. The first element of grid will be the list denoting the top row, the second element will be the list denoting second row from the top, and so on until the last element, which is the list denoting the bottom row. In the list denoting a single row, the first element will be the amount of food the zombie in the left-most room in that row needs, the second element will be the amount the zombie in the room to its immediate right needs and so on. The top left room will always contain the integer 0, to indicate that there is no zombie there.
//
//	The number of rows N will not exceed 20, and the amount of food each zombie requires will be a positive integer not exceeding 10.
//
//	Languages
//	=========
//
//	To provide a Python solution, edit solution.py
//	To provide a Java solution, edit solution.java
//
//	Test cases
//	==========
//
//	Inputs:
//	    (int) food = 7
//	    (int) grid = [	[0, 2, 5], 
//						[1, 1, 3], 
//						[2, 1, 1]	]
//	Output:
//	    (int) 0
//
//	Inputs:
//	    (int) food = 12
//	    (int) grid = [	[0, 2, 5], 
//						[1, 1, 3], 
//						[2, 1, 1]	]
//	Output:
//	    (int) 1
//
//	Use verify [file] to test your solution and see how it does. When you are finished editing your code, use submit [file] to submit your answer. If your solution passes the test cases, it will be removed from your home folder.
	@Test
	public void test_1()
	{
		final int[][] grid = { 	{0, 2, 5},
								{1, 1, 3},
								{2, 1, 1}	};
		final int foodStart = 7;
		
		final int foodLeft = answer(foodStart, grid);
		System.out.println("foodLeft=" + foodLeft);
	}
	
	@Test
	public void test_2()
	{
		final int[][] grid = { 	{0, 2, 5},
								{1, 1, 3},
								{2, 1, 1}	};
		final int foodStart = 12;
		
		final int foodLeft = answer(foodStart, grid);
		System.out.println("foodLeft=" + foodLeft);
	}
	
	@Test
	public void test_3()
	{
		final int[][] grid = { 	
				{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
				{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
				{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
				{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
				{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
				{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
				{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
				{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
				{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
				{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
				{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
				{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
				{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
				{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
				{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
				{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
				{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
				{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
				{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
				{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}
				};
		final int foodStart = 200;
		
		final int foodLeft = answer(foodStart, grid);
		System.out.println("foodLeft=" + foodLeft);
	}
	
	public static int answer(final int food, final int[][] grid) {
		int x = grid.length - 1;
		int y = grid.length - 1;
		
		final Cache cache = new Cache(x*y);
		int remainder = r(food, x, y, grid, cache);
		return remainder <= food ? remainder : -1;
	}
	
	private static int r(final int food, final int x, final int y, final int[][] grid, final Cache cache)
	{
		Integer cacheVal = cache.get(food, x, y);
		if (cacheVal != null)
			return cacheVal;
		
		if (x < 0 || y  < 0)
			return Integer.MAX_VALUE;
		
		final int foodLeft = food - grid[x][y];
		int result;
		if (foodLeft < 0)
			result = Integer.MAX_VALUE;
		else if (x == 0 && y == 0)
			result = foodLeft;
		else
			result = Math.min(r(foodLeft, x - 1, y, grid, cache), r(foodLeft, x, y - 1, grid, cache));
		cache.put(food, x, y, result);
		return result;
	}
	
	public static class Cache {
		
		private final Map<Key, Integer> cache;
		
		public Cache(int capacity) {
			cache = new HashMap<>(capacity);
		}

		public void put(final int food, final int x, final int y, final int foodLeft)
		{
			final Key key = new Key(food, x, y);
			cache.put(key, foodLeft);
		}
		
		public Integer get(final int food, final int x, final int y)
		{
			final Key key = new Key(food, x, y);
			final Integer value = cache.get(key);
			return value;
		}
		
		private class Key {
			private final int food;
			private final int x;
			private final int y;
			
			public Key(int food, int x, int y) {
				this.food = food;
				this.x = x;
				this.y = y;
			}

			@Override
			public int hashCode() {
				final int prime = 31;
				int result = 1;
				result = prime * result + getOuterType().hashCode();
				result = prime * result + food;
				result = prime * result + x;
				result = prime * result + y;
				return result;
			}

			@Override
			public boolean equals(Object obj) {
				if (this == obj)
					return true;
				if (obj == null)
					return false;
				if (getClass() != obj.getClass())
					return false;
				Key other = (Key) obj;
				if (!getOuterType().equals(other.getOuterType()))
					return false;
				if (food != other.food)
					return false;
				if (x != other.x)
					return false;
				if (y != other.y)
					return false;
				return true;
			}

			private Cache getOuterType() {
				return Cache.this;
			}
		}
	}
	/* PYTHON solution
	 * 
	 
class MemoizeFunction:
    def __init__(self, func):
        self.function = func
        self.memory = dict()

    def __call__(self, *args):
        try:
            return self.memory[args]
        except KeyError:
            self.memory[args] = self.function(*args)
            return self.memory[args]

def answer(food, grid):
    @MemoizeFunction
    def r(total, x, y):
        if x < 0 or y < 0:
            return food + 1
        
        foodLeft = total - grid[x][y]
        result = 0
        if foodLeft < 0:
            result = food + 1
        elif x == 0 and y == 0:
            result = foodLeft
        else:
            result = min(r(foodLeft, x - 1, y), r(foodLeft, x, y - 1))
        return result
    
    x = len(grid) - 1
    y = len(grid) - 1
    remainder = r(food, x, y)
    return remainder if remainder <= food else -1
    
	 
	 */
}